package com.example.domain.usecases

import com.example.domain.model.AlbumModel
import com.example.domain.repository.AlbumsDBRepo
import io.reactivex.Completable

class SaveAlbumsUseCaseImpl(private val albumsDBRepo: AlbumsDBRepo) : SaveAlbumsUseCase {
    override fun saveAlbums(albums: List<AlbumModel>): Completable {
        return albumsDBRepo.insert(albums)
    }
}