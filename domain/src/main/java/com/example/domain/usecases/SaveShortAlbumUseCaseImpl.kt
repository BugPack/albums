package com.example.domain.usecases

import com.example.domain.model.ShortAlbumModel
import com.example.domain.repository.ShortAlbumsDBRepo
import io.reactivex.Completable

class SaveShortAlbumUseCaseImpl(private val shortAlbumsDBRepo: ShortAlbumsDBRepo) : SaveShortAlbumUseCase {
    override fun saveShortAlbum(shortAlbum: ShortAlbumModel): Completable {
        return shortAlbumsDBRepo.insert(shortAlbum)
    }
}