package com.example.domain.usecases

import com.example.domain.model.ShortAlbumModel
import com.example.domain.repository.AlbumsRepo
import io.reactivex.Single

class GetShortAlbumsUseCaseImpl(private val albumsRepo: AlbumsRepo) : GetShortAlbumsUseCase {
    override fun getShortAlbums(): Single<List<ShortAlbumModel>> {
        return albumsRepo.getShortAlbums()
    }
}