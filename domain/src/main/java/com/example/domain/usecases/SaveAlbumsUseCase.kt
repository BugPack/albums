package com.example.domain.usecases

import com.example.domain.model.AlbumModel
import io.reactivex.Completable

interface SaveAlbumsUseCase {
    fun saveAlbums(albums: List<AlbumModel>): Completable
}