package com.example.domain.usecases

import androidx.lifecycle.LiveData
import com.example.domain.model.ShortAlbumModel
import com.example.domain.repository.ShortAlbumsDBRepo

class LoadShortAlbumsUseCaseImpl(private val shortAlbumsDBRepo: ShortAlbumsDBRepo) : LoadShortAlbumsUseCase {
    override fun loadShortAlbums(): LiveData<List<ShortAlbumModel>> {
        return shortAlbumsDBRepo.getAll()
    }
}