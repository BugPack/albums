package com.example.domain.usecases

import com.example.domain.model.ShortAlbumModel
import io.reactivex.Single

interface LoadShortAlbumUseCase {
    fun loadShortAlbum(albumId: Int): Single<ShortAlbumModel>
}