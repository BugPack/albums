package com.example.domain.usecases

import androidx.lifecycle.LiveData
import com.example.domain.model.ShortAlbumModel

interface LoadShortAlbumsUseCase {
    fun loadShortAlbums(): LiveData<List<ShortAlbumModel>>
}