package com.example.domain.usecases

import com.example.domain.model.ShortAlbumModel
import io.reactivex.Single

interface GetShortAlbumsUseCase {
    fun getShortAlbums(): Single<List<ShortAlbumModel>>
}