package com.example.domain.usecases

import com.example.domain.model.AlbumModel
import io.reactivex.Single

interface LoadAlbumsByIdUseCase {
    fun loadAlbumsById(albumId: Int): Single<List<AlbumModel>>
}