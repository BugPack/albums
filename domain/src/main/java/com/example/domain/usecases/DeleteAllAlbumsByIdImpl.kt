package com.example.domain.usecases

import com.example.domain.repository.AlbumsDBRepo

class DeleteAllAlbumsByIdImpl(private val albumsDBRepo: AlbumsDBRepo) : DeleteAllAlbumsById {
    override fun deleteAllById(albumId: Int) {
        albumsDBRepo.deleteAllById(albumId)
    }
}