package com.example.domain.usecases

import com.example.domain.model.AlbumModel
import com.example.domain.repository.AlbumsRepo
import io.reactivex.Single

class GetAlbumsUseCaseImpl(
    private val albumsRepo: AlbumsRepo
) : GetAlbumsUseCase {
    override fun getAlbums(albumId: Int): Single<List<AlbumModel>> {
        return albumsRepo.getAlbums(albumId)
    }
}