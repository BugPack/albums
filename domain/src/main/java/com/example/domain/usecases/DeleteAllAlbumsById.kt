package com.example.domain.usecases

interface DeleteAllAlbumsById {
    fun deleteAllById(albumId: Int)
}