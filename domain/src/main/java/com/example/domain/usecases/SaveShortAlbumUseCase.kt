package com.example.domain.usecases

import com.example.domain.model.ShortAlbumModel
import io.reactivex.Completable

interface SaveShortAlbumUseCase {
    fun saveShortAlbum(shortAlbum: ShortAlbumModel): Completable
}