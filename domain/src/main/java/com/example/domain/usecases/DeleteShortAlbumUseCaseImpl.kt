package com.example.domain.usecases

import com.example.domain.model.ShortAlbumModel
import com.example.domain.repository.ShortAlbumsDBRepo

class DeleteShortAlbumUseCaseImpl(private val shortAlbumsDBRepo: ShortAlbumsDBRepo) : DeleteShortAlbumUseCase {
    override fun deleteShortAlbum(shortAlbum: ShortAlbumModel) {
        shortAlbumsDBRepo.delete(shortAlbum)
    }
}