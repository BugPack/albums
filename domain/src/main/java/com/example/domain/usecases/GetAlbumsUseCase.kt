package com.example.domain.usecases

import com.example.domain.model.AlbumModel
import io.reactivex.Single

interface GetAlbumsUseCase {
    fun getAlbums(albumId: Int): Single<List<AlbumModel>>
}