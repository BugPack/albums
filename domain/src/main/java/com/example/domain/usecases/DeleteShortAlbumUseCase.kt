package com.example.domain.usecases

import com.example.domain.model.ShortAlbumModel

interface DeleteShortAlbumUseCase {
    fun deleteShortAlbum(shortAlbum: ShortAlbumModel)
}