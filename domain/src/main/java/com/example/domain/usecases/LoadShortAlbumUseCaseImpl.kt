package com.example.domain.usecases

import com.example.domain.model.ShortAlbumModel
import com.example.domain.repository.ShortAlbumsDBRepo
import io.reactivex.Single

class LoadShortAlbumUseCaseImpl(
    private val shortAlbumsDBRepo: ShortAlbumsDBRepo
) : LoadShortAlbumUseCase {
    override fun loadShortAlbum(albumId: Int): Single<ShortAlbumModel> {
        return shortAlbumsDBRepo.getById(albumId)
    }
}