package com.example.domain.usecases

import com.example.domain.model.AlbumModel
import com.example.domain.repository.AlbumsDBRepo
import io.reactivex.Single

class LoadAlbumsByIdUseCaseImpl(private val albumsDBRepo: AlbumsDBRepo) : LoadAlbumsByIdUseCase {
    override fun loadAlbumsById(albumId: Int): Single<List<AlbumModel>> {
        return albumsDBRepo.getAllById(albumId)
    }
}