package com.example.domain.repository

import com.example.domain.model.AlbumModel
import com.example.domain.model.ShortAlbumModel
import io.reactivex.Single

interface AlbumsRepo {
    fun getShortAlbums(): Single<List<ShortAlbumModel>>
    fun getAlbums(albumId: Int): Single<List<AlbumModel>>
}