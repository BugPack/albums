package com.example.domain.repository

import androidx.lifecycle.LiveData
import com.example.domain.model.ShortAlbumModel
import io.reactivex.Completable
import io.reactivex.Single

interface ShortAlbumsDBRepo {
    fun getAll(): LiveData<List<ShortAlbumModel>>
    fun getById(shortAlbumId: Int): Single<ShortAlbumModel>
    fun insert(shortAlbum: ShortAlbumModel): Completable
    fun delete(shortAlbum: ShortAlbumModel)
}