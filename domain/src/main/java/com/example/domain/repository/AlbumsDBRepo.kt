package com.example.domain.repository

import com.example.domain.model.AlbumModel
import io.reactivex.Completable
import io.reactivex.Single

interface AlbumsDBRepo {
    fun getAll(): Single<List<AlbumModel>>
    fun getAllById(albumId: Int): Single<List<AlbumModel>>
    fun insert(albums: List<AlbumModel>): Completable
    fun delete(albumModel: AlbumModel)
    fun deleteAllById(albumId: Int)
}