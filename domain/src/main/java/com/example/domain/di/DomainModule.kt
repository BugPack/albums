package com.example.domain.di

import com.example.domain.usecases.*
import org.koin.dsl.module

val domainModule = module {
    factory<GetShortAlbumsUseCase> { GetShortAlbumsUseCaseImpl(albumsRepo = get()) }
    factory<GetAlbumsUseCase> { GetAlbumsUseCaseImpl(albumsRepo = get()) }
    factory<SaveShortAlbumUseCase> { SaveShortAlbumUseCaseImpl(shortAlbumsDBRepo = get()) }
    factory<LoadShortAlbumsUseCase> { LoadShortAlbumsUseCaseImpl(shortAlbumsDBRepo = get()) }
    factory<DeleteShortAlbumUseCase> { DeleteShortAlbumUseCaseImpl(shortAlbumsDBRepo = get()) }
    factory<SaveAlbumsUseCase> { SaveAlbumsUseCaseImpl(albumsDBRepo = get()) }
    factory<LoadAlbumsByIdUseCase> { LoadAlbumsByIdUseCaseImpl(albumsDBRepo = get()) }
    factory<LoadShortAlbumUseCase> { LoadShortAlbumUseCaseImpl(shortAlbumsDBRepo = get()) }
    factory<DeleteAllAlbumsById> { DeleteAllAlbumsByIdImpl(albumsDBRepo = get()) }
}