package com.example.domain.model

import java.io.Serializable

data class AlbumModel(
    var albumId: Int,
    var id: Int,
    var title: String,
    var url: String,
    var thumbnailUrl: String
) : Serializable