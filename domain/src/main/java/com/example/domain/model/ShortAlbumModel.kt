package com.example.domain.model

import java.io.Serializable

data class ShortAlbumModel(
    val userId: Int,
    val id: Int,
    val title: String
) : Serializable