package com.example.domain.model

import java.io.Serializable

data class CoordsModel(
    var longitude: Double,
    var latitude: Double,
    var altitude: Double
) : Serializable