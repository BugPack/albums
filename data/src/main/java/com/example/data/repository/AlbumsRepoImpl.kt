package com.example.data.repository

import com.example.data.apiservice.ApiService
import com.example.domain.model.AlbumModel
import com.example.domain.model.ShortAlbumModel
import com.example.domain.repository.AlbumsRepo
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AlbumsRepoImpl(private val apiService: ApiService) : AlbumsRepo {
    override fun getShortAlbums(): Single<List<ShortAlbumModel>> {
        return apiService.getAlbums()
            .subscribeOn(Schedulers.io())
            .flattenAsObservable { it }
            .map { item ->
                ShortAlbumModel(
                    userId = item.userId,
                    id = item.id,
                    title = item.title
                )
            }
            .toList()
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun getAlbums(albumId: Int): Single<List<AlbumModel>> {
        return apiService.getAlbumInfo(albumId)
            .subscribeOn(Schedulers.io())
            .flattenAsObservable { it }
            .map { item ->
                AlbumModel(
                    albumId = item.albumId,
                    id = item.id,
                    title = item.title,
                    url = item.url,
                    thumbnailUrl = item.thumbnailUrl
                )
            }
            .toList()
            .observeOn(AndroidSchedulers.mainThread())
    }
}