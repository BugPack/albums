package com.example.data.repository

import com.example.data.db.AlbumsDao
import com.example.data.db.model.AlbumEntity
import com.example.domain.model.AlbumModel
import com.example.domain.repository.AlbumsDBRepo
import io.reactivex.Completable
import io.reactivex.CompletableEmitter
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AlbumsDBRepoImpl(private val albumsDao: AlbumsDao) : AlbumsDBRepo {
    override fun getAll(): Single<List<AlbumModel>> {
        return albumsDao.getAll()
            .subscribeOn(Schedulers.io())
            .flattenAsObservable { it }
            .map { item -> convertAlbumEntityToAlbumModel(item) }
            .toList()
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun getAllById(albumId: Int): Single<List<AlbumModel>> {
        return albumsDao.getAllById(albumId)
            .subscribeOn(Schedulers.io())
            .flattenAsObservable { it }
            .map { item -> convertAlbumEntityToAlbumModel(item) }
            .toList()
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun insert(albums: List<AlbumModel>): Completable {
        return Completable.create { c: CompletableEmitter ->
            albumsDao.insert(convertListAlbumModelToListAlbumEntity(albums))
            c.onComplete()
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun delete(albumModel: AlbumModel) {
        Completable.create { c: CompletableEmitter ->
            albumsDao.delete(convertAlbumModelToAlbumEntity(albumModel))
            c.onComplete()
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    override fun deleteAllById(albumId: Int) {
        Completable.create { c: CompletableEmitter ->
            albumsDao.deleteAllById(albumId)
            c.onComplete()
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    private fun convertAlbumModelToAlbumEntity(albumModel: AlbumModel): AlbumEntity {
        return AlbumEntity(
            albumId = albumModel.albumId,
            id = albumModel.id,
            title = albumModel.title,
            url = albumModel.url,
            thumbnailUrl = albumModel.thumbnailUrl
        )
    }

    private fun convertAlbumEntityToAlbumModel(albumEntity: AlbumEntity): AlbumModel {
        return AlbumModel(
            albumId = albumEntity.albumId,
            id = albumEntity.id,
            title = albumEntity.title,
            url = albumEntity.url,
            thumbnailUrl = albumEntity.thumbnailUrl
        )
    }

    private fun convertListAlbumModelToListAlbumEntity(list: List<AlbumModel>): List<AlbumEntity> {
        val resultList = arrayListOf<AlbumEntity>()
        list.forEach { resultList.add(convertAlbumModelToAlbumEntity(it)) }
        return resultList
    }
}