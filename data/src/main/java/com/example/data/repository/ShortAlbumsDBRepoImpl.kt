package com.example.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.example.data.db.ShortAlbumsDao
import com.example.data.db.model.ShortAlbumEntity
import com.example.domain.model.ShortAlbumModel
import com.example.domain.repository.ShortAlbumsDBRepo
import io.reactivex.Completable
import io.reactivex.CompletableEmitter
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ShortAlbumsDBRepoImpl(private val shortAlbumsDao: ShortAlbumsDao) : ShortAlbumsDBRepo {
    override fun getAll(): LiveData<List<ShortAlbumModel>> {
        return Transformations.map(shortAlbumsDao.getAll()) { entity ->
            convertListShortEntityToListShortAlbumModel(entity)
        }
    }

    override fun getById(shortAlbumId: Int): Single<ShortAlbumModel> {
        return shortAlbumsDao.getById(shortAlbumId)
            .subscribeOn(Schedulers.io())
            .map { item -> convertShortAlbumEntityToShortAlbumModel(item) }
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun insert(shortAlbum: ShortAlbumModel): Completable {
        return Completable.create { c: CompletableEmitter ->
            shortAlbumsDao.insert(convertShortAlbumModelToShortAlbumEntity(shortAlbum))
            c.onComplete()
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun delete(shortAlbum: ShortAlbumModel) {
        Completable.create { c: CompletableEmitter ->
            shortAlbumsDao.delete(convertShortAlbumModelToShortAlbumEntity(shortAlbum))
            c.onComplete()
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    private fun convertShortAlbumModelToShortAlbumEntity(shortAlbumModel: ShortAlbumModel): ShortAlbumEntity {
        return ShortAlbumEntity(
            userId = shortAlbumModel.userId,
            id = shortAlbumModel.id,
            title = shortAlbumModel.title
        )
    }

    private fun convertShortAlbumEntityToShortAlbumModel(shortAlbumEntity: ShortAlbumEntity): ShortAlbumModel {
        return ShortAlbumModel(
            userId = shortAlbumEntity.userId,
            id = shortAlbumEntity.id,
            title = shortAlbumEntity.title
        )
    }

    private fun convertListShortEntityToListShortAlbumModel(
        list: List<ShortAlbumEntity>
    ): List<ShortAlbumModel> {
        val resultList = arrayListOf<ShortAlbumModel>()
        list.forEach { resultList.add(convertShortAlbumEntityToShortAlbumModel(it)) }
        return resultList
    }
}