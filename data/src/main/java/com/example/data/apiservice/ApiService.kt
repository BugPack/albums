package com.example.data.apiservice

import com.example.data.model.DataAlbumModel
import com.example.data.model.DataShortAlbumModel
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("albums")
    fun getAlbums(): Single<List<DataShortAlbumModel>>

    @GET("photos")
    fun getAlbumInfo(@Query("albumId") albumId: Int): Single<List<DataAlbumModel>>
}