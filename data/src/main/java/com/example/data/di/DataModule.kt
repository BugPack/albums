package com.example.data.di

import androidx.room.Room
import com.example.data.db.AlbumsDatabase
import com.example.data.db.ShortAlbumsDatabase
import com.example.data.repository.AlbumsDBRepoImpl
import com.example.data.repository.AlbumsRepoImpl
import com.example.data.repository.ShortAlbumsDBRepoImpl
import com.example.domain.repository.AlbumsDBRepo
import com.example.domain.repository.AlbumsRepo
import com.example.domain.repository.ShortAlbumsDBRepo
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val dataModule = module {
    single {
        Room.databaseBuilder(
            androidContext(),
            ShortAlbumsDatabase::class.java,
            "ShortAlbumsDatabase"
        ).fallbackToDestructiveMigration()
            .build()
    }
    single { get<ShortAlbumsDatabase>().shortAlbumsDao() }

    single {
        Room.databaseBuilder(
            androidContext(),
            AlbumsDatabase::class.java,
            "AlbumsDatabase"
        ).fallbackToDestructiveMigration()
            .build()
    }
    single { get<AlbumsDatabase>().albumsDao() }

    factory<AlbumsRepo> { AlbumsRepoImpl(apiService = get()) }
    factory<ShortAlbumsDBRepo> { ShortAlbumsDBRepoImpl(shortAlbumsDao = get()) }
    factory<AlbumsDBRepo> { AlbumsDBRepoImpl(albumsDao = get()) }
}