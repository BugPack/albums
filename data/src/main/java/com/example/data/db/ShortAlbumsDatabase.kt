package com.example.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.data.db.model.ShortAlbumEntity

@Database(entities = [ShortAlbumEntity::class], version = 1, exportSchema = false)
abstract class ShortAlbumsDatabase : RoomDatabase() {
    abstract fun shortAlbumsDao(): ShortAlbumsDao
}