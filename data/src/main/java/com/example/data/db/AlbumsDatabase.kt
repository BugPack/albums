package com.example.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.data.db.model.AlbumEntity

@Database(entities = [AlbumEntity::class], version = 1, exportSchema = false)
abstract class AlbumsDatabase : RoomDatabase() {
    abstract fun albumsDao(): AlbumsDao
}