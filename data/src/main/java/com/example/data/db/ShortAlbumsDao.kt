package com.example.data.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.data.db.model.ShortAlbumEntity
import io.reactivex.Single

@Dao
interface ShortAlbumsDao {
    @Query("SELECT * FROM shortalbumentity ORDER BY title ASC")
    fun getAll(): LiveData<List<ShortAlbumEntity>>

    @Query("SELECT * FROM shortalbumentity WHERE id =:shortAlbumId")
    fun getById(shortAlbumId: Int): Single<ShortAlbumEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(shortAlbumEntity: ShortAlbumEntity)

    @Delete
    fun delete(shortAlbumEntity: ShortAlbumEntity)
}