package com.example.data.db

import androidx.room.*
import com.example.data.db.model.AlbumEntity
import io.reactivex.Single

@Dao
interface AlbumsDao {
    @Query("SELECT * FROM albumentity ORDER BY title ASC")
    fun getAll(): Single<List<AlbumEntity>>

    @Query("SELECT * FROM albumentity WHERE albumId =:albumId ORDER BY title ASC")
    fun getAllById(albumId: Int): Single<List<AlbumEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(albumEntityList: List<AlbumEntity>)

    @Delete
    fun delete(albumEntity: AlbumEntity)

    @Query("DELETE FROM albumentity WHERE albumId = :albumId")
    fun deleteAllById(albumId: Int)
}