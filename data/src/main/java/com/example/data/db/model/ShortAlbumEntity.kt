package com.example.data.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
data class ShortAlbumEntity(
    val userId: Int,
    @PrimaryKey
    val id: Int,
    val title: String
) : Serializable