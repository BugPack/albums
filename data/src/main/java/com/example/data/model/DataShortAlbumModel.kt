package com.example.data.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class DataShortAlbumModel(
    @SerializedName("userId") val userId: Int,
    @SerializedName("id") val id: Int,
    @SerializedName("title") val title: String
) : Serializable