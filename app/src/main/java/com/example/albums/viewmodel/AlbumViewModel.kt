package com.example.albums.viewmodel

import android.widget.Toast
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.example.albums.App
import com.example.albums.R
import com.example.albums.navigation.ScreenPool
import com.example.albums.ui.adapter.AlbumImagesAdapter
import com.example.domain.model.ShortAlbumModel
import com.example.domain.usecases.GetAlbumsUseCase
import com.example.domain.usecases.SaveAlbumsUseCase
import com.example.domain.usecases.SaveShortAlbumUseCase
import ru.terrakok.cicerone.Router

class AlbumViewModel(
    private val getAlbumsUseCase: GetAlbumsUseCase,
    private val router: Router,
    private val saveShortAlbumUseCase: SaveShortAlbumUseCase,
    private val saveAlbumsUseCase: SaveAlbumsUseCase
) : ViewModel() {
    val shortAlbum = ObservableField<ShortAlbumModel>()

    val adapter = AlbumImagesAdapter {
        router.navigateTo(ScreenPool.AlbumImageScreen(it))
    }

    fun getAlbums() {
        shortAlbum.get()?.id?.let {
            getAlbumsUseCase.getAlbums(it)
                .subscribe({ list ->
                    adapter.addItems(list)
                }, {
                    Toast.makeText(
                        App.INSTANCE.applicationContext,
                        App.INSTANCE.applicationContext.getString(R.string.error_general),
                        Toast.LENGTH_SHORT
                    ).show()
                })
        }
    }

    fun refresh() {
        getAlbums()
    }

    fun saveAlbum() {
        shortAlbum.get()?.let { saveShortAlbumUseCase.saveShortAlbum(it).subscribe() }
        saveAlbumsUseCase.saveAlbums(adapter.getItems()).subscribe()
    }
}