package com.example.albums.viewmodel

import androidx.lifecycle.ViewModel
import com.example.albums.navigation.ScreenPool
import com.example.albums.ui.adapter.FavoriteAdapter
import com.example.domain.usecases.DeleteAllAlbumsById
import com.example.domain.usecases.DeleteShortAlbumUseCase
import com.example.domain.usecases.LoadShortAlbumsUseCase
import ru.terrakok.cicerone.Router

class FavoritesViewModel(
    private val loadShortAlbumsUseCase: LoadShortAlbumsUseCase,
    private val deleteShortAlbumUseCase: DeleteShortAlbumUseCase,
    private val router: Router,
    private val deleteAllAlbumsById: DeleteAllAlbumsById
) : ViewModel() {
    init {
        loadFavoriteAlbums()
    }

    val adapter = FavoriteAdapter({
        deleteShortAlbumUseCase.deleteShortAlbum(it)
        deleteAllAlbumsById.deleteAllById(it.id)
    }, {
        router.navigateTo(ScreenPool.FavoriteScreen(it.id))
    })

    private fun loadFavoriteAlbums() {
        loadShortAlbumsUseCase.loadShortAlbums().observeForever {
            adapter.addItems(it)
        }
    }
}