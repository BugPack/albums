package com.example.albums.viewmodel

import android.widget.Toast
import androidx.lifecycle.ViewModel
import com.example.albums.App
import com.example.albums.R
import com.example.albums.navigation.ScreenPool
import com.example.albums.ui.adapter.AlbumsAdapter
import com.example.domain.usecases.GetShortAlbumsUseCase
import ru.terrakok.cicerone.Router

class AlbumsViewModel(
    private val getShortAlbumsUseCase: GetShortAlbumsUseCase,
    private val router: Router
) : ViewModel() {
    init {
        loadAlbums()
    }

    val adapter = AlbumsAdapter({}) {
        router.navigateTo(ScreenPool.AlbumScreen(it))
    }

    private fun loadAlbums() {
        getShortAlbumsUseCase.getShortAlbums()
            .subscribe({
                adapter.addItems(it)
            }, {
                Toast.makeText(
                    App.INSTANCE.applicationContext,
                    App.INSTANCE.applicationContext.getString(R.string.error_general),
                    Toast.LENGTH_SHORT
                ).show()
            })
    }

    fun refresh() {
        loadAlbums()
    }
}