package com.example.albums.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel

class LocationViewModel : ViewModel() {
    val location = ObservableField<String>()
}