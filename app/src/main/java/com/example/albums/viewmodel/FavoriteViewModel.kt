package com.example.albums.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.example.albums.navigation.ScreenPool
import com.example.albums.ui.adapter.AlbumImagesAdapter
import com.example.domain.model.ShortAlbumModel
import com.example.domain.usecases.GetAlbumsUseCase
import com.example.domain.usecases.LoadAlbumsByIdUseCase
import com.example.domain.usecases.LoadShortAlbumUseCase
import com.example.domain.usecases.SaveAlbumsUseCase
import ru.terrakok.cicerone.Router

class FavoriteViewModel(
    private val router: Router,
    private val loadAlbumsByIdUseCase: LoadAlbumsByIdUseCase,
    private val loadShortAlbumUseCase: LoadShortAlbumUseCase,
    private val getAlbumsUseCase: GetAlbumsUseCase,
    private val saveAlbumsUseCase: SaveAlbumsUseCase
) : ViewModel() {
    val albumId = ObservableField<Int>()
    val shortAlbum = ObservableField<ShortAlbumModel>(ShortAlbumModel(0, 0, ""))

    val adapter = AlbumImagesAdapter {
        router.navigateTo(ScreenPool.AlbumImageScreen(it))
    }

    fun loadAlbum() {
        loadShortAlbum()
        loadAlbums()
    }

    private fun loadShortAlbum() {
        albumId.get()?.let { id ->
            loadShortAlbumUseCase.loadShortAlbum(id).subscribe({
                shortAlbum.set(it)
            }, {})
        }
    }

    private fun loadAlbums() {
        albumId.get()?.let { id ->
            loadAlbumsByIdUseCase.loadAlbumsById(id).subscribe({
                adapter.addItems(it)
                if (adapter.getItems().isEmpty()) {
                    getAlbums()
                }
            }, {})
        }
    }

    private fun getAlbums() {
        albumId.get()?.let { id ->
            getAlbumsUseCase.getAlbums(id)
                .subscribe({ list ->
                    adapter.addItems(list)
                    saveAlbumsUseCase.saveAlbums(list).subscribe()
                }, {})
        }
    }
}