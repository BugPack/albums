package com.example.albums.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel

class AlbumImageViewModel : ViewModel() {
    val imageUrl = ObservableField<String>()
}