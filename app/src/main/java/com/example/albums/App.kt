package com.example.albums

import android.app.Application
import com.example.albums.di.appModule
import com.example.data.di.dataModule
import com.example.data.di.networkModule
import com.example.domain.di.domainModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {
    companion object {
        lateinit var INSTANCE: App
    }

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(appModules + domainModules + dataModules)
        }
        INSTANCE = this
    }
}

val appModules = listOf(appModule)
val domainModules = listOf(domainModule)
val dataModules = listOf(dataModule, networkModule)