package com.example.albums.navigation

import com.example.albums.ui.fragment.*
import com.example.domain.model.ShortAlbumModel
import ru.terrakok.cicerone.android.support.SupportAppScreen

object ScreenPool {
    class MainScreen : SupportAppScreen() {
        override fun getFragment() = MainFragment.getInstance()
    }

    class AlbumsScreen : SupportAppScreen() {
        override fun getFragment() = AlbumsFragment.getInstance()
    }

    class FavoritesScreen : SupportAppScreen() {
        override fun getFragment() = FavoritesFragment.getInstance()
    }

    class LocationScreen : SupportAppScreen() {
        override fun getFragment() = LocationFragment.getInstance()
    }

    class AlbumScreen(
        private val shortAlbum: ShortAlbumModel
    ) : SupportAppScreen() {
        override fun getFragment() = AlbumFragment.getInstance(shortAlbum = shortAlbum)
    }

    class AlbumImageScreen(var imageUrl: String) : SupportAppScreen() {
        override fun getFragment() = AlbumImageFragment.getInstance(imageUrl)
    }

    class FavoriteScreen(var albumId: Int) : SupportAppScreen() {
        override fun getFragment() = FavoriteFragment.getInstance(albumId)
    }
}