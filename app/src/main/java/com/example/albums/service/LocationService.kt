package com.example.albums.service

import android.annotation.SuppressLint
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.os.IBinder
import android.os.Looper
import com.example.albums.common.Constants
import com.example.albums.common.NotificationUtils
import com.example.domain.model.CoordsModel
import com.google.android.gms.location.*
import java.util.concurrent.TimeUnit

class LocationService : Service() {
    companion object {
        var IS_LOCATION_SERVICE_ACTIVE = false
    }

    private var fusedLocationService: FusedLocationProviderClient? = null
    private var lastLocation: CoordsModel? = null
    private var notificationId = 123
    private var mediaPlayer: MediaPlayer? = null

    private val callback = object : LocationCallback() {
        override fun onLocationResult(p0: LocationResult?) {
            p0?.let {
                if (it.locations.isNotEmpty()) {
                    lastLocation = CoordsModel(
                        longitude = it.locations.last().longitude,
                        latitude = it.locations.last().latitude,
                        altitude = it.locations.last().altitude
                    )
                    lastLocation?.let {
                        if (IS_LOCATION_SERVICE_ACTIVE) {
                            showLocationInfo(it)
                            sendBroadcastLocation(it)
                        }
                    }
                }
            }
        }
    }

    override fun onBind(intent: Intent?): IBinder? = null

    override fun onDestroy() {
        stopMediaPlayer()
        super.onDestroy()
    }

    @SuppressLint("MissingPermission")
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        intent?.let {
            when (it.getSerializableExtra(Constants.LOCATION_SERVICE_COMMAND)) {
                Constants.LOCATION_SERVICE_COMMAND_START -> {
                    startForeground(
                        Constants.LOCATION_SERVICE_ID,
                        lastLocation?.let {
                            NotificationUtils.createLocationNotification(this, it)
                        } ?: run {
                            NotificationUtils.createLocationNotification(this)
                        }
                    )

                    fusedLocationService =
                        LocationServices.getFusedLocationProviderClient(this).apply {
                            requestLocationUpdates(
                                LocationRequest().apply {
                                    interval = TimeUnit.SECONDS.toMillis(3)
                                    fastestInterval = TimeUnit.SECONDS.toMillis(1)
                                    priority = LocationRequest.PRIORITY_HIGH_ACCURACY
                                },
                                callback,
                                Looper.getMainLooper()
                            )
                        }
                    startMediaPlayer()
                    sendBroadcastLocationServiceBtnState()
                    IS_LOCATION_SERVICE_ACTIVE = true
                }
                Constants.LOCATION_SERVICE_COMMAND_STOP -> {
                    sendBroadcastLocationServiceBtnState()
                    IS_LOCATION_SERVICE_ACTIVE = false
                    stopMediaPlayer()
                    clearNotification()
                    stopSelf()
                }
                else -> {
                }
            }
        }
        return START_STICKY
    }

    private fun startMediaPlayer() {
        mediaPlayer = MediaPlayer.create(
            this, resources.getIdentifier("the_x_files", "raw", this.packageName)
        ).apply {
            isLooping = true
            start()
        }
    }

    private fun stopMediaPlayer() {
        mediaPlayer?.stop()
        mediaPlayer = null
    }

    private fun showLocationInfo(coords: CoordsModel) {
        (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).apply {
            notify(
                notificationId,
                NotificationUtils.createLocationNotification(this@LocationService, coords)
            )
        }
    }

    private fun clearNotification() {
        (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).apply {
            cancel(notificationId)
        }
    }

    private fun sendBroadcastLocation(coords: CoordsModel) {
        sendBroadcast(Intent(Constants.LOCATION_BROADCAST_ACTION).apply {
            putExtra(Constants.LOCATION_BROADCAST_RESULT, coords)
        })
    }

    private fun sendBroadcastLocationServiceBtnState() {
        sendBroadcast(Intent(Constants.LOCATION_BROADCAST_ACTION).apply {
            putExtra(Constants.LOCATION_SERVICE_CHECK_BTN_STATE, true)
        })
    }
}