package com.example.albums.di

import com.example.albums.viewmodel.*
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

val appModule = module {
    single { Cicerone.create() }
    single<Router> { get<Cicerone<Router>>().router }
    single<NavigatorHolder> { get<Cicerone<Router>>().navigatorHolder }

    viewModel {
        AlbumsViewModel(
            getShortAlbumsUseCase = get(),
            router = get()
        )
    }
    viewModel {
        FavoritesViewModel(
            loadShortAlbumsUseCase = get(),
            deleteShortAlbumUseCase = get(),
            router = get(),
            deleteAllAlbumsById = get()
        )
    }
    viewModel { LocationViewModel() }
    viewModel {
        AlbumViewModel(
            getAlbumsUseCase = get(),
            router = get(),
            saveShortAlbumUseCase = get(),
            saveAlbumsUseCase = get()
        )
    }
    viewModel { MainViewModel() }
    viewModel { AlbumImageViewModel() }
    viewModel {
        FavoriteViewModel(
            router = get(),
            loadAlbumsByIdUseCase = get(),
            loadShortAlbumUseCase = get(),
            getAlbumsUseCase = get(),
            saveAlbumsUseCase = get()
        )
    }
}