package com.example.albums.common

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import com.example.albums.R
import com.example.domain.model.CoordsModel

object NotificationUtils {
    private fun createLocationNotificationChannel(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "location channel"
            val descriptionText = "description location text"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            (context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).createNotificationChannel(
                NotificationChannel(Constants.LOCATION_SERVICE_CHANNEL, name, importance).apply {
                    description = descriptionText
                }
            )
        }
    }

    fun createLocationNotification(
        context: Context,
        location: CoordsModel? = null
    ): Notification {
        createLocationNotificationChannel(context)
        val builder = NotificationCompat.Builder(context, Constants.LOCATION_SERVICE_CHANNEL)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentText("Location service")
        location?.let {
            builder.setSubText("lat: ${it.latitude} lon: ${it.longitude} alt: ${it.altitude}")
        } ?: run {
            builder.setSubText("lat: xxx lon: xxx alt: xxx")
        }
        return builder.build()
    }
}