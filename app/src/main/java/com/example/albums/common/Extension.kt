package com.example.albums.common

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.albums.R
import com.example.albums.ui.adapter.AlbumImagesAdapter
import com.example.albums.ui.adapter.AlbumsAdapter
import com.example.albums.ui.adapter.FavoriteAdapter
import com.github.chrisbanes.photoview.PhotoView
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso

@BindingAdapter("adapter")
fun RecyclerView.adapter(adapter: AlbumsAdapter) {
    this.adapter = adapter
}

@BindingAdapter("adapterFavorite")
fun RecyclerView.adapterFavorite(adapter: FavoriteAdapter) {
    this.adapter = adapter
}

@BindingAdapter("adapterImages")
fun RecyclerView.adapterImages(adapter: AlbumImagesAdapter) {
    this.adapter = adapter
}

@BindingAdapter("onRefresh")
fun SwipeRefreshLayout.onRefresh(onRefresh: () -> Unit) {
    this.setOnRefreshListener {
        this.isRefreshing = false
        onRefresh()
    }
}

@BindingAdapter("imageUrl")
fun PhotoView.imageUrl(url: String) {
    picassoLoad(url, this)
}

fun picassoLoad(url: String, imageView: ImageView) {
    val urlStr = url.replace("https:", "http:")
    val picasso = Picasso.get()
    picasso.isLoggingEnabled = true
    picasso.load(urlStr)
        .error(R.drawable.ic_load_image_error)
        .placeholder(R.drawable.anim_progress_rotation)
        .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
        .centerInside()
        .fit()
        .into(imageView)
}

@BindingAdapter("textCap")
fun TextView.textCap(text: String) {
    this.text = if (text.isEmpty()) "" else text.capitalize()
}