package com.example.albums.common

object Constants {
    const val ALBUM_ID_KEY = "album_id_key"
    const val ALBUM_SHORT_KEY = "album_short_key"
    const val IMAGE_URL_KEY = "image_url_key"

    const val LOCATION_SERVICE_COMMAND = "command"
    const val LOCATION_SERVICE_COMMAND_START = "command_start"
    const val LOCATION_SERVICE_COMMAND_STOP = "command_stop"
    const val LOCATION_SERVICE_ID = 123
    const val LOCATION_SERVICE_CHANNEL = "location_service_channel"

    const val LOCATION_BROADCAST_ACTION = "com.example.albums.ui.fragment"
    const val LOCATION_BROADCAST_RESULT = "location_broadcast_result"
    const val LOCATION_SERVICE_CHECK_BTN_STATE = "location_service_check_btn_state"
}