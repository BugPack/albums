package com.example.albums.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.albums.R
import com.example.albums.common.picassoLoad
import com.example.domain.model.AlbumModel
import kotlinx.android.synthetic.main.list_item_album.view.*

class AlbumImagesAdapter(
    private val onImageClick: (imgUrl: String) -> Unit
) : RecyclerView.Adapter<AlbumImagesAdapter.AlbumImagesViewHolder>() {
    private val dataset = arrayListOf<AlbumModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = AlbumImagesViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.list_item_album, parent, false)
    )

    override fun getItemCount() = dataset.size

    override fun onBindViewHolder(holder: AlbumImagesViewHolder, position: Int) {
        val item = dataset[position]
        holder.apply {
            picassoLoad(item.thumbnailUrl, image)
            root.setOnClickListener {
                onImageClick(item.url)
            }
        }
    }

    fun addItems(items: List<AlbumModel>) {
        val diffResult = DiffUtil.calculateDiff(AlbumImagesDiffUtil(dataset, items))
        dataset.clear()
        dataset.addAll(items)
        diffResult.dispatchUpdatesTo(this)
    }

    fun getItems() = dataset

    class AlbumImagesViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val image = view.ivImage
        val root = view.rootLayout
    }

    private class AlbumImagesDiffUtil(
        private val oldList: List<AlbumModel>,
        private val newList: List<AlbumModel>
    ) : DiffUtil.Callback() {
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].id == newList[newItemPosition].id
        }

        override fun getOldListSize() = oldList.size

        override fun getNewListSize() = newList.size

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].albumId == newList[newItemPosition].albumId &&
                    oldList[oldItemPosition].title.equals(newList[newItemPosition].title) &&
                    oldList[oldItemPosition].url.equals(newList[newItemPosition].url) &&
                    oldList[oldItemPosition].thumbnailUrl.equals(newList[newItemPosition].thumbnailUrl)
        }
    }
}