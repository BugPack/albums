package com.example.albums.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.example.albums.R
import com.example.albums.databinding.FragmentMainBinding
import com.example.albums.ui.adapter.MainFragmentStateAdapter
import com.example.albums.viewmodel.MainViewModel
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment : Fragment() {
    companion object {
        fun getInstance() = MainFragment()
    }

    private val viewModel: MainViewModel by viewModel()
    var fragmentStateAdapter: FragmentStateAdapter? = null

    lateinit var viewPager2: ViewPager2
    lateinit var tabLayout: TabLayout

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val dbu = DataBindingUtil.inflate<FragmentMainBinding>(
            inflater,
            R.layout.fragment_main,
            container,
            false
        ).apply { viewModel = this@MainFragment.viewModel }.root
        viewPager2 = dbu.findViewById(R.id.viewPager2)
        tabLayout = dbu.findViewById(R.id.tabLayout)
        initViewPagerWithTabs()
        return dbu
    }

    private fun initViewPagerWithTabs() {
        fragmentStateAdapter = MainFragmentStateAdapter(childFragmentManager, lifecycle)
        viewPager2.apply {
            offscreenPageLimit = 3
            adapter = fragmentStateAdapter
        }
        TabLayoutMediator(tabLayout, viewPager2) { tab, position ->
            tab.text = when (position) {
                0 -> getString(R.string.albums)
                1 -> getString(R.string.favorite)
                else -> getString(R.string.location)
            }
        }.attach()
    }
}