package com.example.albums.ui.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.albums.ui.fragment.AlbumsFragment
import com.example.albums.ui.fragment.LocationFragment
import com.example.albums.ui.fragment.FavoritesFragment

class MainFragmentStateAdapter(
    fm: FragmentManager,
    lifecycle: Lifecycle
) : FragmentStateAdapter(fm, lifecycle) {
    override fun getItemCount() = 3

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> AlbumsFragment()
            1 -> FavoritesFragment()
            else -> LocationFragment()
        }
    }
}