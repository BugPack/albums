package com.example.albums.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.albums.R
import com.example.domain.model.ShortAlbumModel
import kotlinx.android.synthetic.main.list_item_short_album.view.*

open class AlbumsAdapter(
    var onButtonClick: (ShortAlbumModel) -> Unit,
    var onItemClick: (ShortAlbumModel) -> Unit
) : RecyclerView.Adapter<AlbumsAdapter.AlbumsViewHolder>() {
    protected val dataset = arrayListOf<ShortAlbumModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = AlbumsViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.list_item_short_album, parent, false)
    )

    override fun getItemCount() = dataset.size

    override fun onBindViewHolder(holder: AlbumsViewHolder, position: Int) {
        val item = dataset[position]
        holder.apply {
            title.text = item.title.capitalize()
            root.setOnClickListener {
                onItemClick(item)
            }
            button.setOnClickListener {
                onButtonClick(item)
            }
            button.visibility = View.GONE
        }
    }

    fun addItems(items: List<ShortAlbumModel>) {
        val diffResult = DiffUtil.calculateDiff(AlbumsDiffUtil(dataset, items))
        dataset.clear()
        dataset.addAll(items)
        diffResult.dispatchUpdatesTo(this)
    }

    class AlbumsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var title = view.tvTitle
        var root = view.rootLayout
        var button = view.btn
    }

    private class AlbumsDiffUtil(
        private val oldList: List<ShortAlbumModel>,
        private val newList: List<ShortAlbumModel>
    ) : DiffUtil.Callback() {
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].id == newList[newItemPosition].id
        }

        override fun getOldListSize() = oldList.size

        override fun getNewListSize() = newList.size

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].userId == newList[newItemPosition].userId &&
                    oldList[oldItemPosition].title.equals(newList[newItemPosition].title)
        }
    }
}