package com.example.albums.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.albums.R
import com.example.albums.common.Constants
import com.example.albums.databinding.FragmentAlbumBinding
import com.example.albums.viewmodel.AlbumViewModel
import com.example.domain.model.ShortAlbumModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class AlbumFragment : Fragment() {
    companion object {
        fun getInstance(shortAlbum: ShortAlbumModel) = AlbumFragment().apply {
            arguments = Bundle().apply {
                putSerializable(Constants.ALBUM_SHORT_KEY, shortAlbum)
            }
        }
    }

    private val viewModel: AlbumViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return DataBindingUtil.inflate<FragmentAlbumBinding>(
            inflater,
            R.layout.fragment_album,
            container,
            false
        ).apply { viewModel = this@AlbumFragment.viewModel }.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            if (it.containsKey(Constants.ALBUM_SHORT_KEY)) {
                viewModel.shortAlbum.set(it.getSerializable(Constants.ALBUM_SHORT_KEY) as ShortAlbumModel)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getAlbums()
    }
}