package com.example.albums.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.albums.R
import com.example.albums.databinding.FragmentAlbumsBinding
import com.example.albums.viewmodel.AlbumsViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class AlbumsFragment : Fragment() {
    companion object {
        fun getInstance() = AlbumsFragment()
    }

    private val viewModel: AlbumsViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return DataBindingUtil.inflate<FragmentAlbumsBinding>(
            inflater,
            R.layout.fragment_albums,
            container,
            false
        ).apply { viewModel = this@AlbumsFragment.viewModel }.root
    }
}