package com.example.albums.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.albums.R
import com.example.albums.common.Constants
import com.example.albums.databinding.FragmentAlbumImageBinding
import com.example.albums.viewmodel.AlbumImageViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class AlbumImageFragment : Fragment() {
    companion object {
        fun getInstance(imageUrl: String) = AlbumImageFragment().apply {
            arguments = Bundle().apply {
                putString(Constants.IMAGE_URL_KEY, imageUrl)
            }
        }
    }

    private val viewModel: AlbumImageViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            if (it.containsKey(Constants.IMAGE_URL_KEY)) {
                viewModel.imageUrl.set(it.getString(Constants.IMAGE_URL_KEY))
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return DataBindingUtil.inflate<FragmentAlbumImageBinding>(
            inflater,
            R.layout.fragment_album_image,
            container,
            false
        ).apply { viewModel = this@AlbumImageFragment.viewModel }.root
    }
}