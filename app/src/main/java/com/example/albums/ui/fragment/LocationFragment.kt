package com.example.albums.ui.fragment

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.albums.R
import com.example.albums.common.Constants
import com.example.albums.databinding.FragmentLocationBinding
import com.example.albums.service.LocationService
import com.example.albums.ui.activity.MainActivity
import com.example.albums.viewmodel.LocationViewModel
import com.example.domain.model.CoordsModel
import kotlinx.android.synthetic.main.fragment_location.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class LocationFragment : Fragment() {
    companion object {
        fun getInstance() = LocationFragment()
    }

    private val viewModel: LocationViewModel by viewModel()

    private var broadcastReceiver: BroadcastReceiver? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initBroadcastReceiver()
    }

    override fun onDestroy() {
        super.onDestroy()
        (activity as? MainActivity)?.unregisterReceiver(broadcastReceiver)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return DataBindingUtil.inflate<FragmentLocationBinding>(
            inflater,
            R.layout.fragment_location,
            container,
            false
        ).apply { viewModel = this@LocationFragment.viewModel }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initClickListeners()
        checkButtonState()
    }

    private fun initBroadcastReceiver() {
        broadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                intent?.let {
                    (it.getSerializableExtra(Constants.LOCATION_BROADCAST_RESULT) as? CoordsModel)?.let {
                        viewModel.location.set("lon: ${it.longitude} \n lat: ${it.latitude} \n alt: ${it.altitude}")
                    }
                    (it.getSerializableExtra(Constants.LOCATION_SERVICE_CHECK_BTN_STATE) as? Boolean)?.let {
                        checkButtonState()
                    }
                }
            }
        }
        (activity as? MainActivity)?.registerReceiver(
            broadcastReceiver,
            IntentFilter(Constants.LOCATION_BROADCAST_ACTION)
        )
    }

    private fun initClickListeners() {
        btnLocation?.setOnClickListener {
            if (LocationService.IS_LOCATION_SERVICE_ACTIVE) {
                stopService()
            } else {
                checkPermissions()
            }
        }
    }

    private fun checkButtonState() {
        if (LocationService.IS_LOCATION_SERVICE_ACTIVE) {
            btnLocation?.text = getText(R.string.stop)
        } else {
            btnLocation?.text = getText(R.string.start)
        }
    }

    private fun checkPermissions() {
        if (ActivityCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ), 0
            )
        } else {
            startService()
        }
    }

    private fun startService() {
        activity?.startService(
            Intent(requireActivity(), LocationService::class.java).apply {
                putExtra(
                    Constants.LOCATION_SERVICE_COMMAND,
                    Constants.LOCATION_SERVICE_COMMAND_START
                )
            }
        )
    }

    private fun stopService() {
        activity?.startService(
            Intent(requireActivity(), LocationService::class.java).apply {
                putExtra(
                    Constants.LOCATION_SERVICE_COMMAND,
                    Constants.LOCATION_SERVICE_COMMAND_STOP
                )
            }
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (permissions.contains(Manifest.permission.ACCESS_FINE_LOCATION) &&
            permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION &&
            grantResults[0] == 0 &&
            permissions.contains(Manifest.permission.ACCESS_COARSE_LOCATION) &&
            permissions[1] == Manifest.permission.ACCESS_COARSE_LOCATION &&
            grantResults[1] == 0
        ) {
            startService()
        }
    }
}