package com.example.albums.ui.adapter

import android.view.View
import androidx.core.content.ContextCompat
import com.example.domain.model.ShortAlbumModel

class FavoriteAdapter(
    onButtonClick: (ShortAlbumModel) -> Unit,
    onItemClick: (ShortAlbumModel) -> Unit
) : AlbumsAdapter(onButtonClick, onItemClick) {

    override fun onBindViewHolder(holder: AlbumsViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        val item = dataset[position]
        holder.apply {
            button.background = ContextCompat.getDrawable(
                button.context, android.R.drawable.ic_delete
            )
            button.visibility = View.VISIBLE
        }
    }
}